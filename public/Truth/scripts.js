var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
var player;

function onYouTubeIframeAPIReady() {
  player = new YT.Player('player', {
    width: '1920',
    height: '1080',
    videoId: 'Lc6W7JbjQd8',          
    playerVars: { 'autoplay': 1, 'controls': 0, 'playlist':'Lc6W7JbjQd8', 'loop': 1, 'modestbranding': 1, 'rel': 0 },
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
}

function onPlayerReady(event) {
  document.body.style.backgroundColor = "black";
  event.target.playVideo();
}

function playerResize() { // initially + also on window resize (should also horizontal + vertical center vid)
  // if window dimensions > 1920x1080
    player.setSize(1920, 1080);
  // if window dimensions > 1600×900
    player.setSize(1600, 900);
  // if window dimensions > 1366×768
    player.setSize(1366, 768);
  // if window dimensions > 1280x720
    player.setSize(1280, 720);
  // if window dimensions > 960×540
    player.setSize(960, 540);
  // if window dimensions > 854×480
    player.setSize(854, 480);
  // if window dimensions > 720×405
    player.setSize(720, 405);
  // if window dimensions > 640×360
    player.setSize(640, 360);
    // if window dimensions > 426x240
    player.setSize(426, 240);
  // if window dimensions > 320x180
    player.setSize(320, 180);
  
}
