const synth = new Tone.Synth().toDestination();
const loop = new Tone.Loop(time => {
  synth.triggerAttackRelease("C4", "8n", time);
  synth.triggerAttackRelease("E4", "8n", "+4n", 0.5);
  synth.triggerAttackRelease("G4", "8n", "+4n", 0.5);
}, "4n");

loop.start(0).stop("4n");
Tone.Transport.start();
